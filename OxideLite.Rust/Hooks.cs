﻿using Harmony;
using Oxide.Core;
using Steamworks;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Reflection.Emit;
using System.Text;
using System.Threading.Tasks;
using UnityEngine;

namespace OxideLite.Rust
{
    //Combine together underscores
    //_ _instance = this
    //_ _ _fieldName = private field
    //_ _1 = argument #1
    //_ _parameterName

    #region Oxide Stuff

    [HarmonyPatch( typeof( Bootstrap ), "Init_Tier0" )]
    public static class InitPatch
    {
        [HarmonyPostfix()]
        static void DoPostfix()
        {
            Interface.Initialize();
        }
    }

    [HarmonyPatch( typeof( Bootstrap ), "StartupShared" )]
    public static class InitLogging
    {
        [HarmonyPrefix()]
        static void Prefix()
        {
            Interface.CallHook( "InitLogging" );
        }
    }

    [HarmonyPatch( typeof( ServerMgr ), "DoTick" )]
    public static class OnTick
    {
        [HarmonyPrefix()]
        static void Prefix()
        {
            Interface.CallHook( "OnTick" );
        }
    }

    [HarmonyPatch( typeof( ServerMgr ), "OpenConnection" )]
    public static class IOnServerInitialized
    {
        [HarmonyPostfix()]
        static void Postfix()
        {
            Interface.CallHook( "IOnServerInitialized" );
        }
    }


    #endregion

    #region Server

    //DoAutomatedSave

    [HarmonyPatch( typeof( SaveRestore ), "DoAutomatedSave" )]
    public static class DoAutomatedSave
    {
        [HarmonyPrefix()]
        static void Hook()
        {
            Interface.CallHook( "DoAutomatedSave" );
        }
    }

    [HarmonyPatch( typeof( ServerMgr ), "Shutdown" )]
    public static class OnServerShutdown
    {
        [HarmonyPrefix()]
        static void Hook()
        {
            Interface.CallHook( "OnServerShutdown" );
        }
    }

    #endregion

    #region Internal

    [HarmonyPatch]
    public class IOnBaseCombatEntityHurt : BaseTranspileHook
    {
        static MethodInfo TargetMethod()
        {
            return AccessTools.Method( "BaseCombatEntity:Hurt" );
        }

        public override void TryInsert( CodeInstruction instruction )
        {
            if ( instruction.CheckMethod( "DebugHurt" ) )
            {
                MoveBeforeMethod();

                CallHook( "IOnBaseCombatEntityHurt", Arg.This(), Arg.Parameter( 0 ) );
            }
        }
    }

    [HarmonyPatch]
    public class IOnUserApprove : BaseTranspileHook
    {
        static MethodInfo TargetMethod()
        {
            return AccessTools.Method( "BaseCombatEntity:Hurt" );
        }

        public override void TryInsert( CodeInstruction instruction )
        {
            if ( instruction.CheckField( "m_AuthConnection" ) )
            {
                MoveBack();

                CallHook( "IOnUserApprove", Return.Exit(), Arg.This(), Arg.Parameter( 0 ) );
            }
        }
    }

    public class IOnLoseCondition : BaseTranspileHook
    {
        public override void TryInsert( CodeInstruction instruction )
        {
            if ( instruction.CheckField( "disablecondition", typeof(bool) ) )
            {
                SearchForward( x => x.opcode == OpCodes.Ret );

                CallHook( "IOnLoseCondition", Arg.This(), Arg.Parameter( 0 ) );
            }
        }
    }

    [HarmonyPatch( typeof( BasePlayer ), "OnAttacked" )]
    public static class IOnBasePlayerAttacked
    {
        [HarmonyPrefix()]
        static bool Hook( BasePlayer __instance, HitInfo __0 )
        {
            if ( Interface.CallHook( "IOnBasePlayerAttacked", __instance, __0 ) != null )
            {
                return false;
            }
            return true;
        }
    }

    [HarmonyPatch( typeof( BasePlayer ), "Hurt", typeof( HitInfo ) )]
    public static class IOnBasePlayerHurt
    {
        [HarmonyPrefix()]
        static bool Hook( BasePlayer __instance, HitInfo __0 )
        {
            if ( Interface.CallHook( "IOnBasePlayerHurt", __instance, __0 ) != null )
            {
                return false;
            }
            return true;
        }
    }

    [HarmonyPatch]
    public static class IOnServerShutdown
    {
        static MethodInfo TargetMethod()
        {
            return AccessTools.Method( "ServerMgr:Shutdown" );
        }

        [HarmonyPrefix()]
        static void Hook()
        {
            Interface.CallHook( "IOnServerShutdown" );
        }
    }

    [HarmonyPatch]
    public static class IOnUpdateServerInformation
    {
        static MethodInfo TargetMethod()
        {
            return AccessTools.Method( "ServerMgr:UpdateServerInformation" );
        }

        [HarmonyPrefix()]
        static void Hook()
        {
            if ( !SteamServer.IsValid )
            {
                return;
            }

            // Presumably some fix
            SteamServer.SetKey( "description_0", string.Empty );
        }

        [HarmonyPostfix()]
        static void Postfix()
        {
            if ( !SteamServer.IsValid )
            {
                return;
            }

            SteamServer.GameTags += ",oxide";
            if ( Interface.Oxide.Config.Options.Modded )
            {
                SteamServer.GameTags += ",modded";
            }
        }
    }

    //"HookName": "IOnPlayerCommand",
    //"HookName": "IOnPlayerChat",
    public class IOnPlayerCommand : BaseTranspileHook
    {
        public override void TryInsert( CodeInstruction instruction )
        {
            if ( instruction.opcode == OpCodes.Ldstr && ( instruction.operand is string text && text == @"\\" ) )
            {
                SearchForward( x => x.opcode == OpCodes.Ret );

                MoveBack();

                CallHook( "IOnPlayerCommand", Arg.Parameter( 4 ), Arg.Parameter( 3 ) );
            }

            if ( instruction.CheckMethod( "EscapeRichText") )
            {
                MoveAfterMethod();

                SearchStoreLocal( SearchDirection.Before, typeof( string ), out var local );

                CallHook( "IOnPlayerChat", Return.Exit(), Arg.Parameter( 1 ), Arg.Parameter( 2 ), Arg.Local( local.LocalIndex ), Arg.Parameter( 0 ), Arg.Parameter( 4 ) );
            }
        }
    }

    //"HookName": "IOnRconInitialize",

    //"HookName": "IOnNpcTarget",
    //"HookName": "IOnNpcTarget",
    //"HookName": "IOnNpcTarget",
    //"HookName": "IOnNpcSenseVision",
    //"HookName": "IOnNpcSenseClose",
    //"HookName": "IOnNpcTarget",


    //"HookName": "IOnPlayerConnected",
    // "HookName": "IOnPlayerBanned",
    // "HookName": "IOnServerInitialized",
    // "HookName": "IOnEntitySaved",
    // "HookName": "IOnServerCommand",
    // "HookName": "IOnRunCommandLine",
    // "HookName": "IOnRconCommand",

    #endregion

    #region Terrain

    [HarmonyPatch( typeof( BaseBoat ), "GenerateOceanPatrolPath" )]
    public static class OnBoatPathGenerate
    {
        [HarmonyPrefix()]
        static bool Prefix(ref List<Vector3> __result )
        {
            object obj = Interface.CallHook( "OnBoatPathGenerate" );
            if ( obj is List<Vector3> )
            {
                __result = ( List<Vector3>)obj;
                return false;
            }
            return true;
        }
    }

    [HarmonyPatch( typeof( World ), "Spawn", typeof(string), typeof(Prefab), typeof( Vector3 ), typeof(Quaternion), typeof(Vector3) )]
    public class OnWorldPrefabSpawned : BaseTranspileHook
    {
        //Copy paste into each transpile hook
        static IEnumerable<CodeInstruction> Transpiler( IEnumerable<CodeInstruction> instructions, ILGenerator generator, MethodBase originalMethod )
        {
            return DoTranspile( MethodBase.GetCurrentMethod().DeclaringType, instructions, generator, originalMethod );
        }

        public override void TryInsert( CodeInstruction instruction )
        {
            if ( !instruction.CheckMethod( "SetHierarchyGroup"))
            {
                return;
            }

            MoveBeforeMethod();

            if ( SearchStoreLocal(SearchDirection.Before, typeof(GameObject), out var local) == null)
            {
                return;
            }

            CallHook( "OnWorldPrefabSpawned", Arg.Local( local.LocalIndex ), Arg.Parameter( 0 ) );
        }
    }

    #endregion

    #region Entity

    #region OnEntityTakeDamage



    #endregion

    [HarmonyPatch( typeof( BaseNetworkable ), "Spawn" )]
    public class OnEntitySpawned : BaseTranspileHook
    {
        //Copy paste into each transpile hook
        static IEnumerable<CodeInstruction> Transpiler( IEnumerable<CodeInstruction> instructions, ILGenerator generator, MethodBase originalMethod )
        {
            return DoTranspile( MethodBase.GetCurrentMethod().DeclaringType, instructions, generator, originalMethod );
        }

        public override void TryInsert( CodeInstruction instruction )
        {
            if ( instruction.opcode == OpCodes.Stfld && instruction.CheckField( typeof( BaseNetworkable ), "isSpawned" ) )
            {
                CallHook( "OnEntitySpawned", Arg.This() );
            }
        }
    }

    [HarmonyPatch( typeof( BaseNetworkable ), "Kill" )]
    public class OnEntityKill : BaseTranspileHook
    {
        //Copy paste into each transpile hook
        static IEnumerable<CodeInstruction> Transpiler( IEnumerable<CodeInstruction> instructions, ILGenerator generator, MethodBase originalMethod )
        {
            return DoTranspile( MethodBase.GetCurrentMethod().DeclaringType, instructions, generator, originalMethod );
        }

        public override void TryInsert( CodeInstruction instruction )
        {
            if ( instruction.CheckMethod( "BroadcastOnParentDestroying" ) )
            {
                MoveAfterMethod();
                CallHook( "OnEntityKill", Arg.This() );
            }
        }
    }

    #endregion

    #region Player

    [HarmonyPatch( typeof( BasePlayer ), "PlayerInit" )]
    public static class OnPlayerInit
    {
        [HarmonyPostfix()]
        static void Postfix( BasePlayer __instance )
        {
            Interface.CallHook( "OnPlayerInit", __instance );
        }
    }

    #region OnDisconnected

    [HarmonyPatch( typeof( ServerMgr ), "OnDisconnected" )]
    public static class OnPlayerDisconnected_Prefix
    {
        //BasePlayer player, string reason
        public static string reason;

        [HarmonyPrefix()]
        static void Prefix( string __1)
        {
            reason = __1;
        }
    }

    [HarmonyPatch( typeof( BasePlayer ), "OnDisconnected" )]
    public static class OnPlayerDisconnected
    {
        [HarmonyPrefix()]
        static void Prefix( BasePlayer __instance )
        {
            Interface.CallHook( "OnPlayerDisconnected", __instance, OnPlayerDisconnected_Prefix.reason );
        }
    }

    #endregion

    #endregion

    #region Chat

    [HarmonyPatch( typeof( ConVar.Chat ), "sayImpl" )]
    public class IOnPlayerChat : BaseTranspileHook
    {
        //Copy paste into each transpile hook
        static IEnumerable<CodeInstruction> Transpiler( IEnumerable<CodeInstruction> instructions, ILGenerator generator, MethodBase originalMethod )
        {
            return DoTranspile( MethodBase.GetCurrentMethod().DeclaringType, instructions, generator, originalMethod );
        }

        public override void TryInsert( CodeInstruction instruction )
        {
            if ( instruction.CheckMethod( "EscapeRichText" ) )
            {
                MoveAfterMethod();
                CallHook( "IOnPlayerChat", Return.Exit(), Arg.Parameter( 0 ), Arg.Parameter( 1 ), Arg.Parameter( 2 ) );
            }
        }
    }

    #endregion

    #region Item

    [HarmonyPatch( typeof( ItemContainer ), "Insert" )]
    public static class OnItemAddedToContainer
    {
        [HarmonyPostfix()]
        static void Postfix( ItemContainer __instance, Item __0 )
        {
            Interface.CallHook( "OnItemAddedToContainer", __instance, __0 );
        }
    }

    [HarmonyPatch( typeof( ItemContainer ), "Remove" )]
    public static class OnItemRemovedFromContainer
    {
        [HarmonyPostfix()]
        static void Postfix( ItemContainer __instance, Item __0 )
        {
            Interface.CallHook( "OnItemRemovedFromContainer", __instance, __0 );
        }
    }

    [HarmonyPatch( typeof( ResourceDispenser ), "GiveResourceFromItem" )]
    public class OnDispenserGather : BaseTranspileHook
    {
        //Copy paste into each transpile hook
        static IEnumerable<CodeInstruction> Transpiler( IEnumerable<CodeInstruction> instructions, ILGenerator generator, MethodBase originalMethod )
        {
            return DoTranspile( MethodBase.GetCurrentMethod().DeclaringType, instructions, generator, originalMethod );
        }

        public override void TryInsert( CodeInstruction instruction )
        {
            //Start at GiveItem(Item)
            if ( instruction.CheckMethod( "GiveItem", typeof( BaseEntity ) ) )
            {
                //Move after OverrideOwnership
                if ( MoveBeforeMethod() )
                {
                    Console.WriteLine( $"Moved before method to instruction {CurrentInstruction()}" );
                    if ( SearchLoadLocal( SearchDirection.After, typeof( Item ), out var local ) != null )
                    {
                        Console.WriteLine( $"Local found" );
                        //Insert hook before GiveItem()
                        CallHook( "OnDispenserGather", Return.Exit(), Arg.This(), Arg.Parameter( 0 ), Arg.Local( local.LocalIndex ) );
                    }
                    else
                    {
                        Console.WriteLine( $"Local not found" );
                        FinishTranspile();
                    }
                }
            }
        }
    }

    [HarmonyPatch( typeof( ResourceDispenser ), "AssignFinishBonus" )]
    public class OnDispenserBonus : BaseTranspileHook
    {
        //Copy paste into each transpile hook
        static IEnumerable<CodeInstruction> Transpiler( IEnumerable<CodeInstruction> instructions, ILGenerator generator, MethodBase originalMethod )
        {
            return DoTranspile( MethodBase.GetCurrentMethod().DeclaringType, instructions, generator, originalMethod );
        }

        public override void TryInsert( CodeInstruction instruction )
        {
            //Start at GiveItem(Item)
            if ( instruction.CheckMethod( "GiveItem", typeof( BaseEntity ) ) )
            {
                //Move after OverrideOwnership
                if ( MoveBeforeMethod() )
                {
                    Console.WriteLine( $"Moved before method to instruction {CurrentInstruction()}" );
                    if ( SearchLoadLocal( SearchDirection.After, typeof( Item ), out var local ) != null )
                    {
                        Console.WriteLine( $"Local found" );
                        //Insert hook before GiveItem()
                        CallHook( "OnDispenserBonus", Return.Exit(), Arg.This(), Arg.Parameter( 0 ), Arg.Local( local.LocalIndex ) );
                    }
                    else
                    {
                        Console.WriteLine( $"Local not found" );
                        FinishTranspile();
                    }
                }
            }
        }
    }

    /*
    [HarmonyPatch( typeof( PlantEntity ), "PickFruit" )]
    public class OnCropGather : BaseTranspileHook
    {
        //Copy paste into each transpile hook
        static IEnumerable<CodeInstruction> Transpiler( IEnumerable<CodeInstruction> instructions, ILGenerator generator, MethodBase originalMethod )
        {
            return DoTranspile( MethodBase.GetCurrentMethod().DeclaringType, instructions, generator, originalMethod );
        }

        public override void TryInsert( CodeInstruction instruction )
        {
            //Start at GiveItem(Item)
            if ( instruction.CheckMethod( "GiveItem", typeof( BaseEntity ) ) == false )
            {
                return;
            }

            MoveBeforeMethod();

            if ( SearchForLocal( SearchDirection.After, typeof( Item ), out var item ) == null )
            {
                FinishTranspile();
                return;
            }

            CallHook( "OnCropGather", Return.Exit(), Arg.This(), Arg.Local( item.LocalIndex ), Arg.Parameter( 0 ) );

            if ( !MoveAfter( ( search ) => { return search.CheckMethod( "GiveItem", typeof( BaseEntity ) ); } ) )
            {
                return;
            }

            MoveBeforeMethod();

            if ( SearchForLocal( SearchDirection.After, typeof( Item ), out var item2 ) == null )
            {
                FinishTranspile();
                return;
            }

            CallHook( "OnCropGather", Return.Exit(), Arg.This(), Arg.Local( item2.LocalIndex ), Arg.Parameter( 0 ) );
        }
    }*/

    [HarmonyPatch( typeof( GrowableEntity ), "PickFruit" )]
    public static class OnCropGather
    {
        public static BasePlayer Player;
        public static GrowableEntity Instance;
        public static bool Active;

        [HarmonyPrefix()]
        static void Prefix( GrowableEntity __instance, BasePlayer __0 )
        {
            Instance = __instance;
            Player = __0;   
        }

        [HarmonyPostfix()]
        static void PostFix( GrowableEntity __instance, BasePlayer __0 )
        {
            Instance = null;
            Player = null;
        }
    }

    [HarmonyPatch( typeof( CollectibleEntity ), "DoPickup" )]
    public class OnCollectiblePickup : BaseTranspileHook
    {
        //Copy paste into each transpile hook
        static IEnumerable<CodeInstruction> Transpiler( IEnumerable<CodeInstruction> instructions, ILGenerator generator, MethodBase originalMethod )
        {
            return DoTranspile( MethodBase.GetCurrentMethod().DeclaringType, instructions, generator, originalMethod );
        }

        public override void TryInsert( CodeInstruction instruction )
        {
            if ( instruction.CheckMethod( "Create", typeof( ItemManager ) ) == false )
            {
                return;
            }

            MoveAfterMethod();

            if ( SearchStoreLocal( SearchDirection.Before, typeof( Item ), out var item ) == null )
            {
                FinishTranspile();
                return;
            }

            CallHook( "OnCollectiblePickup", Return.Exit(), Arg.Local( item.LocalIndex ), Arg.Parameter(0), Arg.This() );
        }
    }

    [HarmonyPatch( typeof( BasePlayer ), "GiveItem" )]
    public static class OnGiveItem
    {
        [HarmonyPrefix()]
        static bool Prefix( BasePlayer __instance, Item __0 )
        {
            if ( OnCropGather.Instance != null && OnCropGather.Active == false)
            {
                OnCropGather.Active = true;
                if (Interface.CallHook( "OnCropGather", OnCropGather.Instance, __0, __instance ) != null)
                {
                    return false;
                }
                OnCropGather.Active = false;
            }
            return true;
        }
    }

    
    [HarmonyPatch( typeof( MiningQuarry ), "ProcessResources" )]
    public class OnQuarryGather : BaseTranspileHook
    {
        //Copy paste into each transpile hook
        static IEnumerable<CodeInstruction> Transpiler( IEnumerable<CodeInstruction> instructions, ILGenerator generator, MethodBase originalMethod )
        {
            return DoTranspile( MethodBase.GetCurrentMethod().DeclaringType, instructions, generator, originalMethod );
        }

        public override void TryInsert( CodeInstruction instruction )
        {
            //Start at GiveItem(Item)
            if ( instruction.CheckMethod( "Create", typeof( ItemManager ) ) == false )
            {
                return;
            }

            MoveAfterMethod();

            if ( SearchStoreLocal( SearchDirection.Before, typeof( Item ), out var item ) == null )
            {
                FinishTranspile();
                return;
            }

            CallHook( "OnQuarryGather", Return.Exit(), Arg.This(), Arg.Local( item.LocalIndex ) );
        }
    }

    [HarmonyPatch( typeof( ExcavatorArm ), "ProduceResources" )]
    public class OnExcavatorGather : BaseTranspileHook
    {
        //Copy paste into each transpile hook
        static IEnumerable<CodeInstruction> Transpiler( IEnumerable<CodeInstruction> instructions, ILGenerator generator, MethodBase originalMethod )
        {
            return DoTranspile( MethodBase.GetCurrentMethod().DeclaringType, instructions, generator, originalMethod );
        }

        public override void TryInsert( CodeInstruction instruction )
        {
            //Start at GiveItem(Item)
            if ( instruction.CheckMethod( "Create", typeof( ItemManager ) ) == false )
            {
                return;
            }

            MoveAfterMethod();

            if ( SearchStoreLocal( SearchDirection.Before, typeof( Item ), out var item ) == null )
            {
                FinishTranspile();
                return;
            }

            CallHook( "OnQuarryGather", Return.Exit(), Arg.This(), Arg.Local( item.LocalIndex ) );
        }
    }

    #endregion
}
