﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace OxideLite.Rust.Hooks
{
    public class HookedMember
    {
        public bool This;
        public ParameterInfo Parameter;
        public FieldInfo Field;
        public LocalVariableInfo Local;
        public PropertyInfo Property;
        public MethodInfo Method;
    }
}
