﻿using Microsoft.CodeAnalysis;
using Microsoft.CodeAnalysis.CSharp;
using Microsoft.CodeAnalysis.Emit;
using Microsoft.CodeAnalysis.Text;
using ObjectStream.Data;
using Oxide.Plugins;
using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading;

namespace Oxide.CSharp
{
    public class RoslynCompiler
    {
		private ConcurrentQueue<CompilationRequest> _newRequests = new ConcurrentQueue<CompilationRequest>();
		private ConcurrentQueue<CompilationRequest> _finishedQueue = new ConcurrentQueue<CompilationRequest>();
        private List<CompilationRequest> _activeRequests = new List<CompilationRequest>();
		private DateTime _lastAliveTime;

        internal void EnqueueCompilation( CompilationRequest request )
        {
			_newRequests.Enqueue( request );
        }

		internal void Start()
		{
			ThreadPool.QueueUserWorkItem( CompileThread );
		}

		private void CompileThread(object context )
        {
			_lastAliveTime = DateTime.UtcNow;

			GetCompileRequests();

			ProcessCompileRequests();

			double delta = DateTime.UtcNow.Subtract( _lastAliveTime ).TotalMilliseconds;

			if ( delta < 100 )
            {
				Thread.Sleep( (int)( 100 - delta) );
            }
        }

		private void GetCompileRequests()
        {
			if ( _newRequests.Count > 0 )
            {
				_activeRequests.AddRange( _newRequests.ToArray() );
            }
        }

		private void ProcessCompileRequests()
        {
            foreach(var request in _activeRequests )
            {
				DoCompile( request );
			}

			_activeRequests.Clear();
        }

		private static CSharpCompilationOptions GetCompileOptions()
		{
			var options = new CSharpCompilationOptions( OutputKind.DynamicallyLinkedLibrary )
				.WithOverflowChecks( true )
				.WithOptimizationLevel( OptimizationLevel.Release )
				.WithMetadataImportOptions( MetadataImportOptions.All )
					;

			//Ignore access modifiers (public, private, etc)
			var topLevelBinderFlagsProperty = typeof( CSharpCompilationOptions ).GetProperty( "TopLevelBinderFlags", BindingFlags.Instance | BindingFlags.NonPublic );
			topLevelBinderFlagsProperty.SetValue( options, (uint)1 << 22 );

			return options;
		}

		private static SyntaxTree Parse( string text, string filename = "", CSharpParseOptions options = null )
		{
			var stringText = SourceText.From( text, Encoding.UTF8 );
			return SyntaxFactory.ParseSyntaxTree( stringText, options, filename );
		}

		private void DoCompile( CompilationRequest compilation )
        {
			List<CompilerFile> sourceFiles = ( from path in compilation.plugins.SelectMany( ( CompilablePlugin plugin ) => plugin.IncludePaths ).Distinct()
										select new CompilerFile( path ) ).ToList();
			sourceFiles.AddRange( compilation.plugins.Select( ( CompilablePlugin plugin ) => new CompilerFile( plugin.ScriptName + ".cs", plugin.ScriptSource ) ) );

			CSharpParseOptions parseOptions = CSharpParseOptions.Default.WithLanguageVersion( LanguageVersion.LatestMajor );

			CSharpCompilationOptions compileOptions = GetCompileOptions();

			CSharpCompilation cSharpCompilation = CSharpCompilation.Create(

				//Name of in-memory dll
				compilation.name,
				//Guid.NewGuid().ToString(),

				//Parse source file into syntax trees
				sourceFiles.Select( x => Parse( compilation.plugins.First().ScriptEncoding.GetString( x.Data ), "", parseOptions ) ).ToArray(),

				//Load references
				// We can cache the references (look at CreateFromFile)
				compilation.references.Select( x => MetadataReference.CreateFromFile( Path.Combine( "RustDedicated_Data", "Managed", x.Value.Name ) ) ).ToArray(),

				//Default options
				compileOptions
				);

			Console.WriteLine( "Doing compiling now" );

			try
			{
				using ( var dllStream = new MemoryStream() )
				{
					using ( var pdbStream = new MemoryStream() )
					{
						//Actually compile and write to memory stream
						EmitResult result = cSharpCompilation.Emit( dllStream, pdbStream );

						if ( result.Success )
						{
							Console.WriteLine( "Plugin was compiled successfully!" );
						}
						else
						{
							List<string> errors = result.Diagnostics.Where( x => x.Severity == DiagnosticSeverity.Error )
																	.Select( x => x.ToString() )
																	.ToList();

							compilation.Errors = errors;
						}
					}

					//OnMessage( CompilerMessageType.Compile, compilation, stream.ToArray(), string.Join( "\n", result.Diagnostics.Select( x => x.ToString() ).ToArray() ) );
				}
			}
			catch ( Exception ex )
			{
				Console.WriteLine( $"Compiling plugin caused an error:" );
				Console.WriteLine( ex );
			}

			_finishedQueue.Enqueue( compilation );
		}

        internal CompilationRequest[] GetCompletedRequests()
        {
            if ( _finishedQueue.Count == 0 )
            {
				return Array.Empty<CompilationRequest>();
            }

            var finished = _finishedQueue.ToArray();

			return finished;
        }
    }
}
