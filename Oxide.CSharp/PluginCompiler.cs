﻿
using ObjectStream;
using ObjectStream.Data;
using Oxide.Core;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Net;
using System.Security.Cryptography;
using System.Text.RegularExpressions;
using System.Threading;
using Oxide.CSharp;

namespace Oxide.Plugins
{
    public class PluginCompiler
    {
        public static bool AutoShutdown = true;
        private int lastId;
        private readonly Regex fileErrorRegex = new Regex( @"([\w\.]+)\(\d+\,\d+\+?\): error|error \w+: Source file `[\\\./]*([\w\.]+)", RegexOptions.Compiled );

        private RoslynCompiler _compiler; 
     
        private Hash<int, CompilationRequest> compilations;

        public PluginCompiler()
        {
            compilations = new Hash<int, CompilationRequest>();
            _compiler = new RoslynCompiler();
            _compiler.Start();
        }

        private void GetCompletedRequests()
        {
            var finished = _compiler.GetCompletedRequests();

            if (finished.Length == 0 )
            {
                return;
            }

            foreach ( var request in finished )
            {

            }
        }

        internal void Compile(CompilablePlugin[] plugins, Action<CompilationRequest> callback)
        {
            int id = lastId++;
            CompilationRequest compilation = new CompilationRequest(id, callback, plugins);
            compilations[id] = compilation;
            compilation.Prepare(() => EnqueueCompilation(compilation));
        }

        private void EnqueueCompilation(CompilationRequest compilation)
        {
            if (compilation.plugins.Count < 1)
            {
                //Interface.Oxide.LogDebug("EnqueueCompilation called for an empty compilation");
                return;
            }

            compilation.Started();
            //Interface.Oxide.LogDebug("Compiling with references: {0}", compilation.references.Keys.ToSentence());
            //List<CompilerFile> sourceFiles = compilation.plugins.SelectMany(plugin => plugin.IncludePaths).Distinct().Select(path => new CompilerFile(path)).ToList();
            //sourceFiles.AddRange(compilation.plugins.Select(plugin => new CompilerFile($"{plugin.ScriptName}.cs", plugin.ScriptSource)));
            ////Interface.Oxide.LogDebug("Compiling files: {0}", sourceFiles.Select(f => f.Name).ToSentence());
            //CompilerData data = new CompilerData
            //{
            //    OutputFile = compilation.name,
            //    SourceFiles = sourceFiles.ToArray(),
            //    ReferenceFiles = compilation.references.Values.ToArray()
            //};
            //CompilerMessage message = new CompilerMessage { Id = compilation.id, Data = data, Type = CompilerMessageType.Compile };

            _compiler.EnqueueCompilation( compilation );
        }

        internal void OnCompileSucceeded( CompilationRequest compilation, byte[] dllBytes, byte[] pdbBytes )
        {
            compilation.endedAt = Interface.Oxide.Now;
            foreach ( string line in compilation.Errors )
            {
                Match match = fileErrorRegex.Match( line.Trim() );
                for ( int i = 1; i < match.Groups.Count; i++ )
                {
                    string value = match.Groups[ i ].Value;
                    if ( value.Trim() == string.Empty )
                    {
                        continue;
                    }

                    string fileName = value.Basename();
                    string scriptName = fileName.Substring( 0, fileName.Length - 3 );
                    CompilablePlugin compilablePlugin = compilation.plugins.SingleOrDefault( pl => pl.ScriptName == scriptName );
                    if ( compilablePlugin == null )
                    {
                        Interface.Oxide.LogError( $"Unable to resolve script error to plugin: {line}" );
                        continue;
                    }
                    IEnumerable<string> missingRequirements = compilablePlugin.Requires.Where( name => !compilation.IncludesRequiredPlugin( name ) );
                    if ( missingRequirements.Any() )
                    {
                        compilablePlugin.CompilerErrors = $"Missing dependencies: {missingRequirements.ToSentence()}";
                    }
                    else
                    {
                        compilablePlugin.CompilerErrors = line.Trim().Replace( Interface.Oxide.PluginDirectory + Path.DirectorySeparatorChar, string.Empty );
                    }
                }
            }
            compilation.Completed( dllBytes, pdbBytes );
            compilations.Remove( compilation.id );
        }

        internal void OnCompileFailed( CompilationRequest request, string error )
        {
            Interface.Oxide.LogError( "Compilation error: {0}", error );
            compilations[ request.id ].Completed();
            compilations.Remove( request.id );
        }

        private static void OnError(Exception exception) => Interface.Oxide.LogException("Compilation error: ", exception);

        private static string EscapePath(string path)
        {
            if (string.IsNullOrEmpty(path))
            {
                return "\"\"";
            }

            path = Regex.Replace(path, @"(\\*)" + "\"", @"$1\$0");
            path = Regex.Replace(path, @"^(.*\s.*?)(\\*)$", "\"$1$2$2\"");
            return path;
        }

        private static class Algorithms
        {
            public static readonly HashAlgorithm MD5 = new MD5CryptoServiceProvider();
            public static readonly HashAlgorithm SHA1 = new SHA1Managed();
            public static readonly HashAlgorithm SHA256 = new SHA256Managed();
            public static readonly HashAlgorithm SHA384 = new SHA384Managed();
            public static readonly HashAlgorithm SHA512 = new SHA512Managed();
        }

        private static string GetHash(string filePath, HashAlgorithm algorithm)
        {
            using (BufferedStream stream = new BufferedStream(File.OpenRead(filePath), 100000))
            {
                byte[] hash = algorithm.ComputeHash(stream);
                return BitConverter.ToString(hash).Replace("-", string.Empty).ToLower();
            }
        }
    }
}
