﻿using Oxide.Core.Libraries.Covalence;
using Oxide.Game.Rust;
using System;
using System.Collections.Generic;
using System.Text;

namespace Oxide
{
    public static class BasePlayerEx
    {
        public static IPlayer IPlayer( this BasePlayer player )
        {
            return RustCore.Covalence.PlayerManager.FindPlayerById( player.UserIDString );
        }
    }
}
